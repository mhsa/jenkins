node('master') {

	stage('Obtiene métodos'){

     def path_config= env.JENKINS_HOME+'/workspace'

     println "===== Obteniendo Jenkins pipeline ====="
       sh '''
          cd '''+path_config+'''
          git clone https://mhsa@bitbucket.org/mhsa/jenkins.git || true

          sleep 2
          cd jenkins

          git pull

          cp -r Metodos '''+env.WORKSPACE+'''
         '''
  }
  
  stage('Port forward'){
    //Cargando metodos para todos los stages   
    def metodos = load('Metodos/Metodos.groovy')
    sh'''
    nohup sudo minikube dashboard & pwd

    sleep 10

    nohup sudo kubectl proxy --address='0.0.0.0' --disable-filter=true & 
    '''
    def ipPublica= metodos.obtieneIpPublica()
    println '===== Acceso público ====='
    println 'http://'+ipPublica+':8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/'
    println '===== Acceso local ====='
    println 'http://localhast:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/'
  }  
}
return this; 