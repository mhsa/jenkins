node('master') {
    sh """
      cd /var/run/libvirt/
      sudo chown master:master libvirt-sock
  	  minikube start --driver=kvm2
 	  
 	  """
      echo "======== UP ========";
}
return this;