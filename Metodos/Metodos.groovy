def limpiaDocker(){
  def PATH_SSH_MINIKUBE = sh (script: ' minikube ssh-key ', returnStdout: true).trim()
  def IP_MINIKUBE = sh (script: ' minikube ip ', returnStdout: true).trim()
  sh """
      sleep 5

      sudo docker container prune -f

      sleep 5

      sudo docker image prune -f

      sleep 2

      ssh -i ${PATH_SSH_MINIKUBE} docker@${IP_MINIKUBE} docker container prune -f

      sleep 5

      ssh -i ${PATH_SSH_MINIKUBE} docker@${IP_MINIKUBE} docker image prune -f

      sleep 2

      exit

    """
}

def buildCode(){
  if (env.COMPILER=="GRADLE") {
    sh"""
    cd ${WORKSPACE}
    /opt/gradle/gradle-6.6.1/bin/gradle build
  """
  }else{
    sh """
      cd ${WORKSPACE}   
      mvn clean install
    """
  }
}


def buildDocker(){
  sh """
      cd ${WORKSPACE}
      sudo docker build -t ${DOCKERHUB}/'${REPO}:${BRANCH_NAME}' .

    """
}

def pushDocker(){
  sh """  
      sudo docker login -u="sieven" -p="jenkins.123"

      sleep 5
      sudo docker push ${DOCKERHUB}/'${REPO}:${BRANCH_NAME}'

    """
}

def deployKubernetes(){
  sh """
      cd ${WORKSPACE}
      cd ..
      git clone https://mhsa@bitbucket.org/mhsa/config.git || true
      cd config
      git pull

      sleep 2

      kubectl -n ic apply -f ${REPO}-${BRANCH_NAME}.yaml 

    """
  reinicioPod()  
}

def reinicioPod(){

  def pods = sh (script: ''' set +x
          kubectl get pods -l app='''+REPO+'''-'''+BRANCH_NAME+''' --namespace ic -o custom-columns=:metadata.name | awk '{print $1";"$3}' | tail -n1
        ''', returnStdout: true).trim().split(";")
    def pod = pods[0]
    sh "kubectl -n ic delete pod/"+pod
    
    
    println "----- UP -----"

}

def portAccess(){

  sh """sudo nohup kubectl port-forward --address 0.0.0.0 svc/${REPO}-${BRANCH_NAME} ${env.PORT_FORWARDING}:${env.REAL_PORT} --namespace ic &"""

  sleep 5


  println "===== UP ====="
}

def portKill(){
  try{
  def port = sh (script: ''' sudo lsof -t -i :'''+env.PORT_FORWARDING+''' -s TCP:LISTEN ''', returnStdout: true).trim()
  sh "sudo kill -9 "+port
  }catch (Exception err){
    println err
  }
  
}

def obtieneNombrePod(){
  sh (script: ''' kubectl get pods -l app='''+REPO+'''-'''+BRANCH_NAME+''' -o custom-columns=:metadata.name --namespace ic ''', returnStdout: true).trim()
}

def respaldaImagenesWiki(){

  sh''' mkdir respwikiimages || true '''
  sleep 2
  sh''' rsync -a '''+WORKSPACE+'''/src/images respwikiimages '''
  sleep 2
  sh''' rm -fr '''+WORKSPACE+'''/src/images'''

  def pod = obtieneNombrePod()

  sh'''
  kubectl cp ic/'''+pod+''':/var/www/html/images '''+WORKSPACE+'''/src/images/
  '''
}

def obtieneIpPublica(){
  sh (script: ''' dig +short myip.opendns.com @resolver1.opendns.com ''', returnStdout: true).trim()
}



return this;
