def call() {

 def stages = "${Stage}".split(';')
 env.REPO=env.GIT_URL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')
 env.DOCKERHUB="sieven"
 //def REGISTRY="localhost:5000"
 
 println "------- Obteniendo Jenkins pipeline ---------"
  sh """
    cd ..
    git clone https://mhsa@bitbucket.org/mhsa/jenkins.git || true

    sleep 2
    cd jenkins

    git pull

    cp -r Metodos ${WORKSPACE}
    """
 //Cargando metodos para todos los stages   
 def metodos = load('Metodos/Metodos.groovy')


  stage('BuildCode'){
    if(stages.contains('BuildCode') || "${Stage}" ==''){
      if (env.PROJECT_TYPE=='JAVA') {
        metodos.buildCode()
      }else{
        println '====== No aplica BUILD o no se asignó variable ====='
      }
    }
  }


  stage('BuildDocker'){

    if (env.WIKI=='true') {
    metodos.respaldaImagenesWiki()
    }  
    if(stages.contains('BuildDocker') || "${Stage}" ==''){
    metodos.buildDocker()
    //sudo docker tag ${DOCKERHUB}/'${REPO}:${BRANCH}' ${REGISTRY}/'${REPO}:${BRANCH}'
    }
  }

  stage('CleanOldImages'){
    if(stages.contains('CleanOldImages') || "${Stage}" ==''){  
    metodos.limpiaDocker()
    }
  }

  stage('PushDocker'){
    if(stages.contains('PushDocker') || "${Stage}" ==''){
    metodos.pushDocker()
    //sudo docker push ${REGISTRY}/'${REPO}:${BRANCH}'

    }
  }
  stage('DeployKubernetes'){
    if(stages.contains('DeployKubernetes') || "${Stage}" ==''){
    //sudo docker rmi ${REGISTRY}/'${REPO}:${BRANCH}'
    metodos.deployKubernetes()
      if(env.PORT_FORWARDING && env.REAL_PORT){
        metodos.portKill()
        sleep 45
        metodos.portAccess()
      }
    }
  }
}
return this;