def call() {
 stage('Build Docker'){
     def WORKSPACE=env.WORKSPACE
     def REPO=env.GIT_URL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')
     def BRANCH=env.BRANCH_NAME
     sh """
      cd ${WORKSPACE}
 	  sudo docker build -t sieven/'${REPO}:${BRANCH}' .
 	  """
      echo "Funcionó!";
  }
}
return this;