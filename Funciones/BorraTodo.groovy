node('master') {
    println "${params.Componente}"
    def COMPONENTE = params.Componente
    def RAMA = params.Rama
    def NAMESPACE = params.Namespace
    def DOCKERHUB = "sieven"


    def path_ssh_minikube = sh (script: ' minikube ssh-key ', returnStdout: true).trim()
    def ip_minikube = sh (script: ' minikube ip ', returnStdout: true).trim()

    println "Eliminando deployment..."
    sh """
 	  kubectl delete -n ${NAMESPACE} deployment ${COMPONENTE}-${RAMA} || true
 	  """

    println "Eliminando service..."
    sh """
 	  kubectl delete -n ${NAMESPACE} service ${COMPONENTE}-${RAMA} || true
 	  """
 	println "Eliminando replica controller..." 
 	sh """
 	kubectl delete -n ${NAMESPACE} replicationcontroller ${COMPONENTE}-${RAMA} || true
 	"""

 	sleep 7

 	println "Eliminando contenedor local..."
 	sh """
 	sudo docker container prune -f
 	"""
 	sleep 3

 	println "Eliminando imagen local..."
 	sh """
 	sudo docker rmi ${DOCKERHUB}/${COMPONENTE}:${RAMA} || true
 	"""

 	println "Eliminando contenedor VM..."
 	sh "ssh -i "+path_ssh_minikube+" docker@"+ip_minikube+" 'docker container prune -f' "

 	sleep 5

 	println "Eliminando imagen VM..."
 	sh "ssh -i "+path_ssh_minikube+" docker@"+ip_minikube+" 'docker rmi ${DOCKERHUB}/${COMPONENTE}:${RAMA}' || true"

}
return this;