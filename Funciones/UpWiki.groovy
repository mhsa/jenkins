node('master') {

	stage('Obtiene métodos'){

		 def path_config= env.JENKINS_HOME+'/workspace'

 		 println "===== Obteniendo Jenkins pipeline ====="
  		 sh '''
          cd '''+path_config+'''
          git clone https://mhsa@bitbucket.org/mhsa/jenkins.git || true

          sleep 2
          cd jenkins

          git pull

          cp -r Metodos '''+env.WORKSPACE+'''
         '''
    }

    stage('Port forward'){
    	  //Cargando metodos para todos los stages   
		 def metodos = load('Metodos/Metodos.groovy') 
		 env.PORT_FORWARDING=8082
		 env.REAL_PORT=80
		 env.REPO="wiki"
		 env.BRANCH_NAME="master"
		 metodos.portKill()
		 sleep 10
		 metodos.portAccess()
		 def ipPublica= metodos.obtieneIpPublica()
		 println '===== Acceso público ====='
		 println 'http://'+ipPublica+':'+env.PORT_FORWARDING
    }
}
return this;