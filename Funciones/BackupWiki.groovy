node('master') {

	stage('Obtiene métodos'){

		 def path_config= env.JENKINS_HOME+'/workspace'

 		 println "===== Obteniendo Jenkins pipeline ====="
  		 sh '''
          cd '''+path_config+'''
          git clone https://mhsa@bitbucket.org/mhsa/jenkins.git || true

          sleep 2
          cd jenkins

          git pull

          cp -r Metodos '''+env.WORKSPACE+'''
         '''
    }

    stage('Backup'){
    	  //Cargando metodos para todos los stages   
		 def metodos = load('Metodos/Metodos.groovy') 
		 env.REPO = "mysql"
		 env.BRANCH_NAME= "5-7"
		 def pod = metodos.obtieneNombrePod()

		 sh'''
		 kubectl exec '''+pod+''' --namespace ic -- mysqldump -u root -pmy-super-secret-password sentra_wiki > wiki.sql
		 '''
     println '===== Hecho ====='
    }
}
return this;