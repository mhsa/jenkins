def call() {
 stage('Deploy kubernetes'){
     def WORKSPACE=env.WORKSPACE
     def REPO=env.GIT_URL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')
     def BRANCH=env.BRANCH_NAME
     sh """
      cd ${WORKSPACE}/kubernetes
      kubectl apply -f ${REPO}-${BRANCH}.yaml 
 	  """
      echo "Funcionó!";
  }
}
return this;